package com.hcl.oxbank.controller;
import org.springframework.beans.factory.annotation.Autowired;   
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.hcl.oxbank.dto.EMIdto;
import com.hcl.oxbank.entity.Customer;
import com.hcl.oxbank.entity.Loan;
import com.hcl.oxbank.service.CustomerService;
import com.hcl.oxbank.service.EMIService;
import com.hcl.oxbank.service.LoanService;

@RestController 
public class EMIController {
	@Autowired
	private CustomerService customerService;
	@Autowired
	private EMIService emiService;
	@Autowired
	private LoanService loanService;
	@PostMapping("/EMI")
	public String calculateEMI(@RequestBody EMIdto emidto)
	{
		Customer customer = customerService.getDataByID(emidto.getId());
		double emiAmount = emiService.calculateEMIAmount(emidto.getLoanAmount(), emidto.getTenure());
		
		Loan loan = new Loan();
		//loan.setId(emidto.getId);
		loan.setTenure(emidto.getTenure());
		loan.setLoanAmount(emidto.getLoanAmount());
		loan.setCustomers(customer);		  
				
		Loan savedLoan = loanService.saveLoanDetails(loan);
		
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Your monthly EMI amount is ")
		             .append(emiAmount)
	                 .append(" for the period of ")
	                 .append(emidto.getTenure())
		             .append(" years for the requested loan amount ")
	                 .append(emidto.getLoanAmount());
		String result = stringBuilder.toString();
		
		return result;
		
	}	

}
