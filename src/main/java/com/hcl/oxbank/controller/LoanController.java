package com.hcl.oxbank.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.oxbank.entity.Loan;
import com.hcl.oxbank.service.LoanService;

@RestController
public class LoanController {

	@Autowired
	LoanService loanService;
	
	@PostMapping("/addLoan")
	public String saveLoanDetails(@RequestBody Loan loan)
	{
		 loanService.saveLoanDetails(loan);
		 return "Your account number is:"+loan.getLoanAccountNumber()+"with loan Amount"+loan.getLoanAmount()+"for the id"+loan.getCustomers().getID();
	}
}
