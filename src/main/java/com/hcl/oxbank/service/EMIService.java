package com.hcl.oxbank.service;
import org.springframework.stereotype.Service;

@Service
public class EMIService {
	
	public double calculateEMIAmount(double loanAmount, int numberOfYears)
	{
		
		double interestRate = 0.12;
		double monthlyInterestRate = interestRate / 12;
		int numberOfMonths = numberOfYears * 12;
		double emiAmount = (1000000 * monthlyInterestRate * Math.pow(1 + monthlyInterestRate, numberOfMonths)) / (Math.pow(1 + monthlyInterestRate, numberOfMonths) - 1);
		return emiAmount;
	}

}
